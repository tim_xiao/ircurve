# Zero Curve Construction Guide

The term structure of interest rates, also known as zero curve, is defined as the relationship between the zero-to-maturity on a zero coupon bond and the bond’s maturity. Zero zero curves play an essential role in the valuation of all financial products.
 
Zero curves can be derived from government bonds or LIBOR/swap instruments. The LIBOR/swap term structure offers several advantages over government curves, and is a robust tool for pricing and hedging financial products.  Correlations among governments and other fixed-income products have declined, making the swap term structure a more efficient hedging and pricing vehicle.

Summary

	Zero Curve Introduction
	Zero Curve Construction and Bootstrapping Overview
	Interpolation
	Optimization
•	Zero Curve

Zero Curve Introduction

	The term structure of interest rates, also known as zero curve, is defined as the relationship between the zero-to-maturity on a zero coupon bond and the bond’s maturity.
	Zero zero curves play an essential role in the valuation of all financial products.
	Zero curves can be derived from government bonds or LIBOR/swap instruments.
	The LIBOR/swap term structure offers several advantages over government curves, and is a robust tool for pricing and hedging financial products. 
	Correlations among governments and other fixed-income products have declined, making the swap term structure a more efficient hedging and pricing vehicle.
	With the supply of government issues declining, LIBOR/swap markets are more liquid and efficient than government debt markets.
	LIBOR curves constructed from the most liquid interest rate instruments have become the standard funding curves in the market.
	The 3 month LIBOR curve is the base zero curve in the market.
	The term structure of zero rates is constructed from a set of market quotes of some liquid market instruments such as short term cash instruments, middle term futures or forward rate agreement (FRA), long term swaps and spreads. 
	Prior to the 2007 financial crisis, financial institutions performed valuation and risk management of any interest rate derivative on a given currency using a single-curve approach. This approach consisted of building a unique curve and using it for both discounting and forecasting cashflows.
	However, after the financial crisis, basis swap spreads were no longer negligible and the market was characterized by a sort of segmentation. Consequently, market practitioners started to use a new valuation approach referred to as multicurve approach, which is characterized by a unique discounting curve and multiple forecasting curves
	The current methodology in capital markets for marking to market securities and derivatives is to estimate and discount future cash flows using rates derived from the appropriate term structure. The zero term structure is increasingly used as the foundation for deriving relative term structures and as a benchmark for pricing and hedging.

Zero Curve Construction Overview

	Zero curves are derived or bootstrapped from observed market instruments that represent the most liquid and dominant interest rate products for certain time horizons.
	Normally the curve is divided into three parts. The short end of the term structure is determined using LIBOR rates. The middle part of the curve is constructed using Eurodollar futures or forward rate agreements (FRA). The far end is derived using mid swap rates.
	The objective of the bootstrap algorithm is to find the zero zero or discount factor for each maturity point and cash flow date sequentially so that all curve instruments can be priced back to the market quotes.
	All bootstrapping methods build up the term structure from shorter maturities to longer ones.
	One needs to have valuation models for each instrument.
	Given a Future price, the zero or zero rate can be directly calculated as
𝑟=(100−𝑃)/100−𝐶𝑣𝑥𝐴𝑑𝑗/10000
where
	P	the quoted interest rate Future price
	r	the derived zero or zero rate
	CvxAdj	the convexity adjustment quoted in basis points (bps)

	The swap pricing model is introduced as https://finpricing.com/lib/IrSwap.html
	Assuming that we have all zeros up to 4 years and now need to derive up to 5 years. 
•	Let x be the zero at 5 years.
•	Use an interpolation methd to get zeros at 4.25, 4.5 and 4.75 years as Ax, Bx, Cx,Dx.
•	Given the 5 year market swap rate, we can use a root-finding algorithm to solve the x that makes the value of the 5 year inception swap equal to zero.
•	Therefore we get all zeros or equivalent discount factors up to 5 years
	Repeat the above procedure till the longest swap maturity.
	There are two keys in zero curve construction: interpolation and root finding.

Interpolation

	Most popular interpolation algorithms in curve bootstrapping are linear, log-linear and cubic spline.
	The selected interpolation rule can be applied to either zero rates or discount factors.
	Some critics argue that some of these simple interpolations cannot generate smooth forward rates and the others may be able to produce smooth forward rates but fail to match the market quotes.		
	Also they cannot guarantee the continuity and positivity of forward rates.
	The monotone convex interpolation is more rigorous. It meets the following essential criteria:
•	Replicate the quotes of all input underlying instruments.
•	Guarantee the positivity of the implied forward rates
•	Produce smooth forward curves.
	Although the monotone convex interpolation rule sounds almost perfectly, it is not very popular with practitioners.

Optimization

	As described above, the bootstrapping process needs to solve a zero using a root finding algorithm. 
	In other words, it needs an optimization solution to match the prices of curve-generated instruments to their market quotes.
	FinPricing employs the Levenberg-Marquardt algorithm for root finding, which is very common in curve construction.
	Another popular algorithm is the Excel Solver, especially in Excel application.



You can find more details at
https://finpricing.com/lib/IrCurveIntroduction.html
